function routerConfig($stateProvider, $urlRouterProvider, toppings, stuffing) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('main', {
            component: 'mainPage',
            name: 'mainPage',
            url: '/'
        })
        .state('toppings', {
            component: 'tableHamburger',
            name: 'toppings',
            resolve: {
                title: function () {
                    return 'Toppings';
                },
                items: ['toppings', function (toppings) {
                    return toppings;
                }]
            },
            url: '/toppings'
        })
        .state('stuffings', {
            component: 'tableHamburger',
            name: 'stuffings',
            resolve: {
                title: function () {
                    return 'Stuffings';
                },
                items: ['stuffing', function (stuffing) {
                    return stuffing;
                }]
            },
            url: '/stuffings'
        })
        .state('selectSize', {
            component: 'selectSize',
            name: 'selectSize',
            url: '/select-size'
        })
        .state('hamburgerFilling', {
            component: 'hamburgerFilling',
            name: 'hamburgerFilling',
            url: '/filling'
        })
        .state('orderHistory', {
            component: 'orderHistory',
            name: 'orderHistory',
            url: '/history'
        })
}

routerConfig.$inject = ['$stateProvider', '$urlRouterProvider', 'toppings', 'stuffing'];

angular
    .module('hamburger', [
        'ui.bootstrap',
        'ui.router',
        'hamburger.main-page',
        'hamburger.select-size',
        'hamburger.filling',
        'hamburger.filler-table',
        'hamburger.count',
        'hamburger.order-info',
        'hamburger.constants',
        'hamburger.service',
        'hamburger.order-history-service',
        'hamburger.bill',
        'hamburger.table',
        'hamburger.header',
        'hamburger.order-history',
        'hamburger.confirm'
    ])
    .config(routerConfig);
