const sizes = {
    SIZE_SMALL: {
        code: 'Small',
        calories: 100,
        price: 5,
        size: 5
    },
    SIZE_LARGE: {
        code: 'Large',
        calories: 200,
        price: 10,
        size: 10
    }
};

const stuffing = {
    STUFFING_CHEESE: {
        code: 'Cheese',
        calories: 70,
        price: 7
    },
    STUFFING_SALAD: {
        code: 'Salad',
        calories: 10,
        price: 3
    },
    STUFFING_POTATO: {
        code: 'Potato',
        calories: 50,
        price: 4
    }
};

const toppings = {
    TOPPING_MAYO: {
        code: 'Mayo',
        calories: 120,
        price: 4
    },
    TOPPING_SPICE: {
        code: 'Spice',
        calories: 10,
        price: 2
    }
};

(function () {
    angular
        .module('hamburger.constants', [])
        .constant('sizes', sizes)
        .constant('stuffing', stuffing)
        .constant('toppings', toppings)
})();
