(function () {
    class OrderHistoryService {
        constructor(hamburgerService) {
            this._hamburgerService = hamburgerService;
            this._orderHistory = [];
        }

        addToHistory() {
            const itemToHistory = {
                date: Date.now(),
                size: this._hamburgerService.getSize(),
                toppings: this._hamburgerService.getToppings(),
                stuffing: this._hamburgerService.getStuffing(),
                count: this._hamburgerService.getCount(),
                total: this._hamburgerService.getCount() * this._hamburgerService.calculatePrice()
            };
            this._orderHistory.push(itemToHistory);
        }

        getHistory() {
            return this._orderHistory;
        }
    }

    OrderHistoryService.$inject = ['hamburgerService'];

    angular
        .module('hamburger.order-history-service', [])
        .service('orderHistory', OrderHistoryService)
})();
