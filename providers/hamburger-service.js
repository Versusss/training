(function () {
    class HamburgerService {
        constructor() {
            this._addedStuff = [];
            this._addedTopping = [];
            this._size = {};
            this._count = 1;
            this._created = false;
        }

        createHamburger(sizeInput, stuffing) {
            this._size = sizeInput;
            if (stuffing) {
                this._addedStuff.push(stuffing);
            }
            this._created = true;
        }

        addStuffing(item) {
            const hasDuplicates = this._addedStuff.some(function (element) {
                return element.code === item.code;
            });
            if (hasDuplicates) {
                console.log('Ошибка, нельзя добавить повторяющиеся начинки');
                return;
            }
            if (this._addedStuff.length >= this._size.size) {
                console.log('Нельзя добавить больше ' + this._size.size + 'начинок');
                return;
            }
            this._addedStuff.push(item);
        }

        addTopping(item) {
            const hasDuplicates = this._addedTopping.some(function (element) {
                return element.code === item.code;
            });
            if (hasDuplicates) {
                console.log('Ошибка, нельзя добавить повторяющиеся топпинги');
                return;
            }
            this._addedTopping.push(item);
        }

        removeTopping(item) {
            this._addedTopping = this._addedTopping.filter(function (element) {
                return element !== item;
            });
        }

        removeStuffing(item) {
            this._addedStuff = this._addedStuff.filter(function (element) {
                return element !== item;
            });
        }

        getStuffing() {
            return this._addedStuff;
        }

        getToppings() {
            return this._addedTopping;
        }

        getSize() {
            return this._size;
        }

        calculateByParam(param) {
            const reduceToSum = function (array) {
                return array.reduce(function (elementSum, element) {
                    return elementSum + element[param];
                }, 0);
            };
            return this._size[param] +
                reduceToSum(this._addedStuff) +
                reduceToSum(this._addedTopping);
        }

        sumByProperty(property) {
            return function (accumulator, element) {
                return accumulator + element[property];
            };
        }

        calculateCalories() {
            return this._size.calories +
                this._addedStuff.reduce(this.sumByProperty('calories'), 0) +
                this._addedTopping.reduce(this.sumByProperty('calories'), 0);
        }

        calculatePrice() {
            return this._size.price +
                this._addedStuff.reduce(this.sumByProperty('price'), 0) +
                this._addedTopping.reduce(this.sumByProperty('price'), 0);
        }

        clearAll() {
            this._addedStuff = [];
            this._addedTopping = [];
            this._size = {};
            this._count = 1;
            this._created = false;
        }

        isCreated() {
            return this._created;
        }

        setCount(newVal) {
            this._count = newVal;
        }

        getCount() {
            return this._count;
        }
    }

    angular
        .module('hamburger.service', [])
        .service('hamburgerService', HamburgerService)
})();
