(function () {
    class HamburgerFillingController {
        constructor(toppings, stuffing, hamburgerService, orderHistory, $uibModal, $state) {
            this.toppings = toppings;
            this.stuffing = stuffing;
            this.$uibModal = $uibModal;
            this.$state = $state;
            this.hamburgerService = hamburgerService;
            this.orderHistory = orderHistory;
        }

        payForOrder() {
            const self = this;
            const modalInstance = this.$uibModal.open({
                animation: true,
                component: 'bill',
                size: 'sm',
                resolve: {
                    title: function () {
                        return 'Совершить покупку?';
                    },
                    toppings: function () {
                        return self.hamburgerService.getToppings();
                    },
                    stuffing: function () {
                        return self.hamburgerService.getStuffing();
                    },
                    size: function () {
                        return self.hamburgerService.getSize();
                    },
                    price: function () {
                        return self.hamburgerService.calculatePrice();
                    },
                    counter: function () {
                        return self.hamburgerService.getCount();
                    },
                    okButton: function () {
                        return true;
                    }
                }
            });

            modalInstance.result.then(function () {
                console.log('Вы оплатили заказ');
                self.orderHistory.addToHistory();
                self.hamburgerService.clearAll();
                self.$state.go('main');
            }, function () {
                console.log('Вы отменили заказ');
            });
        };
    }

    HamburgerFillingController.$inject = ['toppings', 'stuffing', 'hamburgerService', 'orderHistory', '$uibModal', '$state'];

    const hamburgerFillingComponent = {
        controller: HamburgerFillingController,
        controllerAs: 'vm',
        templateUrl: 'components/hamburger-filling/hamburger-filling.tpl.html'
    };

    angular
        .module('hamburger.filling', [])
        .component('hamburgerFilling', hamburgerFillingComponent);
})();
