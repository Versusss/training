(function () {
    class CountController {
        constructor() {
        }

        $onInit() {
            this.countValue = this.hamburger.getCount();
            this.countSum();
        }

        countSum() {
            this.totalSum = this.hamburger.calculatePrice() * this.hamburger.getCount();
        }

        changeCount() {
            this.hamburger.setCount(this.countValue);
            this.countSum();
        }

        $onChanges() {
            this.countSum();
        }
    }

    const countComponent = {
        bindings: {
            hamburger: '<',
            price: '<'
        },
        controller: CountController,
        controllerAs: 'vm',
        templateUrl: 'components/count/count.tpl.html'
    };

    angular
        .module('hamburger.count', [])
        .component('count', countComponent);
})();
