(function () {
    class MainPageController {
        constructor() {
        }
    }

    const mainPageComponent = {
        controller: MainPageController,
        controllerAs: 'vm',
        templateUrl: 'components/main-page/main-page.tpl.html',
    };

    angular
        .module('hamburger.main-page', [])
        .component('mainPage', mainPageComponent)
})();
