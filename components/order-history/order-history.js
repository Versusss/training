(function () {
    class OrderHistoryController {
        constructor(hamburgerService, orderHistory, $uibModal) {
            this.hamburgerService = hamburgerService;
            this.orderHistory = orderHistory;
            this.$uibModal = $uibModal;
        }

        openComponentModal(arg) {
            const item = arg;
            const modalInstance = this.$uibModal.open({
                animation: true,
                component: 'bill',
                size: 'sm',
                resolve: {
                    title: function () {
                        return 'Чек';
                    },
                    toppings: function () {
                        return item.toppings;
                    },
                    stuffing: function () {
                        return item.stuffing;
                    },
                    size: function () {
                        return item.size;
                    },
                    price: function () {
                        return item.total;
                    },
                    counter: function () {
                        return item.count;
                    },
                    okButton: function () {
                        return false;
                    }
                }
            });

            modalInstance.result.then(function () {
                angular.noop;
            }, function () {
                angular.noop;
            });
        };
    }

    OrderHistoryController.$inject = ['hamburgerService', 'orderHistory', '$uibModal'];

    const orderHistoryComponent = {
        controller: OrderHistoryController,
        controllerAs: 'vm',
        templateUrl: 'components/order-history/order-history.tpl.html',
    };

    angular
        .module('hamburger.order-history', [])
        .component('orderHistory', orderHistoryComponent)
})();
