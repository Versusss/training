(function () {
    class TableController {
        constructor() {
        }
    }

    const tableComponent = {
        bindings: {
            items: '<',
            title: '@'
        },
        controller: TableController,
        controllerAs: 'vm',
        templateUrl: 'components/table/table.tpl.html'
    };

    angular
        .module('hamburger.table', [])
        .component('tableHamburger', tableComponent);
})();
