(function () {
    class FillerTableController {
        constructor() {
        }
    }

    const fillerTableComponent = {
        bindings: {
            dropdownList: '<',
            fillerList: '<',
            onFillerAdd: '&',
            onFillerRemove: '&',
            title: '@'
        },
        controller: FillerTableController,
        controllerAs: 'vm',
        templateUrl: 'components/filler-table/filler-table.tpl.html'
    };

    angular
        .module('hamburger.filler-table', [])
        .component('fillerTable', fillerTableComponent);
})();
