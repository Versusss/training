(function () {
    class HeadController {
        constructor(hamburgerService, $state, $uibModal) {
            this.hamburgerService = hamburgerService;
            this.$state = $state;
            this.$uibModal = $uibModal;
        }

        openComponentModal() {
            const self = this;
            const modalInstance = this.$uibModal.open({
                animation: true,
                component: 'confirm',
                size: 'sm'
            });

            modalInstance.result.then(function () {
                console.log('Вы отменили заказ');
                self.hamburgerService.clearAll();
                self.$state.go('main');
            }, function () {
                console.log('Вы остались на странице');
            });
        }

        goToMainPage() {
            if (this.hamburgerService.isCreated()) {
                this.openComponentModal();
            } else {
                this.$state.go('main');
            }

        }
    }

    HeadController.$inject = ['hamburgerService', '$state', '$uibModal'];

    const headComponent = {
        controller: HeadController,
        controllerAs: 'vm',
        templateUrl: 'components/header/header.tpl.html',
    };

    angular
        .module('hamburger.header', [])
        .component('headerHamburger', headComponent)
})();
