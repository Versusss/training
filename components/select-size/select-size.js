(function () {
    class SelectSizeController {
        constructor(sizes, hamburgerService, $state) {
            this.sizes = sizes;
            this.hamburgerService = hamburgerService;
            this.$state = $state;
        }

        createHamburger (size) {
            this.hamburgerService.createHamburger(size);
            this.$state.go('hamburgerFilling');
        }

    }

    SelectSizeController.$inject = ['sizes', 'hamburgerService', '$state'];

    const selectSizeComponent = {
        controller: SelectSizeController,
        controllerAs: 'vm',
        templateUrl: 'components/select-size/select-size.tpl.html'
    };

    angular
        .module('hamburger.select-size', [])
        .component('selectSize', selectSizeComponent);
})();
