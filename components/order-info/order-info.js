(function () {
    class OrderInfoController {
        constructor() {
        }
    }

    const orderInfo = {
        bindings: {
            hamburger: '<'
        },
        controller: OrderInfoController,
        controllerAs: 'vm',
        templateUrl: 'components/order-info/order-info.tpl.html'
    };

    angular
        .module('hamburger.order-info', [])
        .component('orderInfo', orderInfo);
})();
