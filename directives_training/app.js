angular.module('myApp', [])
    .controller('myAppController', function ($scope) {
        $scope.callback = function (message) {
            console.log(message);
        }
    })
    .directive('collapser', function () {
        return {
            link: function (scope) {
                scope.isVisible = true;
                scope.show = 'show';
                scope.hide = 'hide';
                scope.changeVisibility = function () {
                    scope.isVisible = !scope.isVisible;
                };
            },
            restrict: 'A',
            scope: {},
            template: '<button ng-click="changeVisibility()" class="btn btn-primary">{{isVisible ? hide : show}}</button>' +
            '<div ng-transclude ng-show="isVisible" ></div>',
            transclude: true,
        };
    })
    .directive('collapser2', function ($compile) {
        return {
            link: function (scope, elem, attrs) {
                scope.isVisible = true;
                scope.setVisible = function () {
                    scope.isVisible = true;
                    if (attrs.collapserOnCollapse) {
                        const message = 'shown';
                        eval('scope.$parent.' + attrs.collapserOnCollapse)
                    }
                };
                scope.setHidden = function () {
                    scope.isVisible = false;
                    if (attrs.collapserOnCollapse) {
                        const message = 'hidden';
                        eval('scope.$parent.' + attrs.collapserOnCollapse)
                    }
                };
                if (attrs.collapserButtonTitle) {
                    const objectWithTitle = eval("(" + attrs.collapserButtonTitle + ")");
                    scope.show = objectWithTitle.show;
                    scope.hide = objectWithTitle.hide;
                } else {
                    scope.show = 'show';
                    scope.hide = 'hide';
                }

                if (attrs.collapserButtonTitle) {
                    // collapserButtonTitle: '=?',
                    //     collapserOnCollapse: '&?'
                    console.log();
                }
            },
            restrict: 'A',
            scope: {},
            template: '<input type="button" ng-value="show" ng-click="setVisible()" class="btn btn-primary">' +
            '<input type="button" ng-value="hide" ng-click="setHidden()" class="btn btn-primary">' +
            '<div ng-transclude ng-if="isVisible" ></div>',
            transclude: true
        };
    })
    .directive('converter', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                ctrl.$formatters.push(function (modelValue) {
                    if (modelValue) {
                        return modelValue.toString();
                    }
                });
                ctrl.$parsers.push(function (viewValue) {
                    if (viewValue) {
                        return viewValue.split();
                    }
                });
            }
        }
    })
    .directive('dateConverter', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                const stringToObject = function (string) {
                    const dateArray = string.split('.');
                    const dateObj = {};
                    if (dateArray.length === 3) {
                        dateObj.day = Number(dateArray[0]);
                        dateObj.month = months[dateArray[1] - 1];
                        dateObj.year = Number(dateArray[2]);
                        return dateObj;
                    } else {
                        console.log('неверный формат даты');
                    }
                };

                const objectToString = function (object) {
                    const month = months.indexOf(object.month) + 1;
                    return object.day + '.' + month + '.' + object.year;
                };

                ctrl.$formatters.push(function (modelValue) {
                    if (modelValue) {
                        return objectToString(modelValue);
                    }
                });
                ctrl.$parsers.push(function (viewValue) {
                    if (viewValue) {
                        return stringToObject(viewValue)
                    }
                });
                ctrl.$validators.validCharacters = function (modelValue, viewValue) {
                    return /^\d\d\.\d\d\.\d\d\d\d$/.test(viewValue);
                };
            }
        }
    });
