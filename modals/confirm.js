(function () {
    class ConfirmController {
        constructor() {
        }

        ok() {
            this.close({$value: 'ok'});
        }

        cancel() {
            this.dismiss({$value: 'cancel'});
        }
    }

    const confirmComponent = {
        bindings: {
            close: '&',
            dismiss: '&'
        },
        controller: ConfirmController,
        controllerAs: 'vm',
        templateUrl: 'modals/confirm.tpl.html'
    };

    angular
        .module('hamburger.confirm', [])
        .component('confirm', confirmComponent);
})();
