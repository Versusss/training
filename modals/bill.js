(function () {
    class BillController {
        constructor() {
        }

        ok() {
            this.close({$value: 'ok'});
        }

        cancel() {
            this.dismiss({$value: 'cancel'});
        }
    }

    const billComponent = {
        bindings: {
            close: '&',
            dismiss: '&',
            resolve: '<',
        },
        controller: BillController,
        controllerAs: 'vm',
        templateUrl: 'modals/bill.tpl.html'
    };

    angular
        .module('hamburger.bill', [])
        .component('bill', billComponent);
})();
